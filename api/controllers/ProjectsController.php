<?php
namespace api\controllers;

use Yii;
use common\models\Project;
use yii\web\Response;

class ProjectsController extends \yii\rest\ActiveController
{
    public $modelClass = 'common\models\Project';

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['delete'], $actions['create'], $actions['index'], $actions['view'], $actions['error']);

        return $actions;
    }

    public function actionIndex()
    {
        $projects = [];
        $list = Project::getAllProjects(true);
        foreach ($list as $project) {
            $projects = [
                'id' => intval($project['id']),
                'name' => $project['name'],
                'date' => Yii::$app->formatter->asDatetime($project['date']),
                'created' => Yii::$app->formatter->asDatetime($project['created_at']),
                'status' => $project['status'],
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $projects;
    }

    public function actionView($id)
    {
        $project = Project::getProjectData($id);
        $imagesCount = Project::getProjectImagesCount($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'id' => intval($project['id']),
            'name' => $project['name'],
            'body' => $project['body'],
            'date' => Yii::$app->formatter->asDatetime($project['date']),
            'created' => Yii::$app->formatter->asDatetime($project['created_at']),
            'updated' => Yii::$app->formatter->asDatetime($project['updated_at']),
            'imagesCount' => intval($imagesCount),
        ];
    }

    public function actionImages($id)
    {
        $images = [];
        $preview = Yii::$app->request->get('preview');
        $preview = !empty($preview) ? trim(strtolower($preview)) : 'original';
        $list = Project::getAllProjectImages($id, $preview);
        foreach ($list as $image) {
            $images[] = [
                'id' => intval($image['id']),
                'title' => $image['title'],
                'description' => $image['description'],
                'path' => Yii::$app->params['host'] . (($preview != 'original') ? $image['preview_image'] : $image['original_image']),
                'stauts' => $image['status'],
            ];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return $images;
    }
}
