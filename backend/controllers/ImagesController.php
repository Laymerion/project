<?php

namespace backend\controllers;

use Yii;
use common\models\Image;
use common\models\ImageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ImagesController implements the CRUD actions for Image model.
 */
class ImagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Image models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => Image::statusesList(),
        ]);
    }

    /**
     * Displays a single Image model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Image model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Image();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $image = $model->uploadImage();
            $model->path = '';
            if ($image) {
                $model->path = $model->getNewPath($image);
                $post['Image']['file'] = $image;
            }
            if ($model->load($post) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'labels' => Image::getStatusLables(),
        ]);
    }

    /**
     * Updates an existing Image model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->path;

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $image = $model->uploadImage();
            if ($image) {
                $model->path = $model->getNewPath($image);
            }
            if ($model->load($post) && $model->save()) {
                if ($image) {
                    Image::removeFile($oldImage);
                    $model->upload($image);
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'labels' => Image::getStatusLables(),
        ]);
    }

    /**
     * Deletes an existing Image model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $get = Yii::$app->request->get();
        $model = $this->findModel($id);
        Image::removeFile($model->path);
        $model->delete();
        

        $redirectPath = ['index'];
        if (!empty($get['model']) && !empty($get['action'])) {
            $redirectPath = ['/' . $get['model'] . '/' . $get['action']];
            if (!empty($get['model_id'])) {
                $redirectPath['id'] = $get['model_id'];
            }
        }

        return $this->redirect($redirectPath);
    }

    /**
     * Finds the Image model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
