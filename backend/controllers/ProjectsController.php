<?php

namespace backend\controllers;

use Yii;
use common\models\Project;
use common\models\ProjectSearch;
use common\models\ProjectImage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;

/**
 * ProjectsController implements the CRUD actions for Project model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'gallery'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'statuses' => Project::statusesList(),
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'statuses' => Project::statusesList(),
        ]);
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($model->load($post) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'statuses' => Project::statusesList(),
        ]);
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGallery($id)
    {
        $model = $this->findModel($id);
        $weights = [];
        foreach ($model->projectImages as $image) {
            $weights[] = $image->image_id;
        }
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $images = UploadedFile::getInstances($model, 'imagesList');
            if ($model->load($post) && $model->save()) {
                foreach ($images as $image) {
                    if (!empty($image)) {
                        $model->uploadImage($image);
                    }
                }
                Yii::$app->session->setFlash('success', Yii::t('app', 'Загружено фотографий: {count}', [
                    'count' => sizeof($images),
                ]));
            }
            if (!empty($post['sorting'])) {
                $sorting = explode(',', $post['sorting']);
                foreach ($sorting as $weight => $imageId) {
                    /* @var $projectImage ProjectImage */
                    $projectImage = ProjectImage::find()
                        ->where([
                            'project_id' => $model->id,
                            'image_id' => $imageId,
                        ])
                        ->one();
                    $projectImage->setAttributes([
                        'weight' => $weight,
                        'title' => $post['title-' . $imageId],
                        'description' => $post['description-' . $imageId],
                    ]);
                    $projectImage->save();
                }
                Yii::$app->session->setFlash('success', Yii::t('app', 'Сортировка сохранена'));
            }
            return $this->redirect(['gallery', 'id' => $model->id]);
        }

        return $this->render('gallery-page', [
            'model' => $model,
            'weights' => implode(',', $weights),
        ]);
    }
}
