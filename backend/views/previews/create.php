<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Preview */

$this->title = Yii::t('app', 'Создание превью');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Превью'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preview-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'labels' => $labels,
    ]) ?>

</div>
