<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Preview */

$this->title = Yii::t('app', 'Редактирование: ') . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Превью'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="preview-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'labels' => $labels,
    ]) ?>

</div>
