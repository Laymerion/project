<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php if (!$model->isNewRecord): ?>
        <p>
            <?= Html::a('<i class="glyphicon glyphicon-th"></i> ' . Yii::t('app', 'Галерея'), ['gallery', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::className(), [
        'options' => ['placeholder' => Yii::t('app', 'Enter a date...')],
        'readonly' => true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true,
            'todayBtn' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 10, 'class' => 'rich-editor']) ?>

    <?= $form->field($model, 'status')->dropDownList($statuses) ?>

    <?php
    /*$preview = [];
    if (!empty($model->imagesList)) {
        foreach ($model->imagesList as $image) {
            $preview[] = $image->path;
        }
    }
    echo $form->field($model, 'imagesList[]')->widget(FileInput::className(), [
        'language' => 'ru',
        'options' => [
            'multiple' => true,
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'initialPreview' => $preview,
            'previewFileType' => 'image',
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('app', 'Выберите изображения'),
        ],
        
    ]);*/
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Назад'), $model->isNewRecord ? ['index'] : ['view', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
