<?php
use yii\helpers\Html;
use kartik\widgets\FileInput;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
    'options' => [
        'enctype'=>'multipart/form-data',
    ]
]); ?>
<div class="form-group">
    <?php
    echo $form->field($model, 'imagesList[]')->widget(FileInput::className(), [
        'language' => 'ru',
        'options' => [
            'multiple' => true,
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  Yii::t('app', 'Выберите изображения'),
        ],
        
    ]);
    ?>
</div>
<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Загрузить'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>