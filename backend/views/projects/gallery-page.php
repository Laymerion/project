<?php

use yii\helpers\Html;
use kartik\sortinput\SortableInput;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = Yii::t('app', 'Галерея проекта');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Проекты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-gallery">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Collapse::widget([
        'items' => [
            [
                'label' => Yii::t('app', 'Добавить изображения'),
                'content' => $this->render('add-images', [
                    'model' => $model,
                ]),
                'contentOptions' => [],
            ],
        ],
    ]) ?>

    <?= Html::beginForm('', 'post', [
        'id' => 'gallery-sort-form',
    ]); ?>
    <?php
    $images = [];
    foreach ($model->projectImages as $projectImage):
        $images[$projectImage->image_id] = [
            'content' => '<div class="elements project-gallery-sorting">' . 
                '<div class="element img">' . Html::img($projectImage->image->path, [
                    'title' => $projectImage->title,
                    'alt' => $projectImage->title,
                    'class' => 'gallery-image',
                ]) . '</div>' . 
                '<div class="element title">' . Html::textInput('title-' . $projectImage->image_id, $projectImage->title, [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Заголовок'),
                ]) . '</div>' . 
                '<div class="element description">' . Html::textarea('description-' . $projectImage->image_id, $projectImage->description, [
                    'class' => 'form-control',
                    'placeholder' => Yii::t('app', 'Описание'),
                ]) . '</div>' . 
                '<div class="element remove">' . Html::a('<span class="glyphicon glyphicon-trash remove-relation"></span>', [
                    '/images/delete',
                    'id' => $projectImage->image_id,
                    'model' => 'projects',
                    'action' => 'gallery',
                    'model_id' => $model->id,
                ], [
                    'class' => 'remove-img',
                    'data' => [
                        'confirm' => Yii::t('app', 'Вы уверены что хотите удалить этот объект?'),
                        'method' => 'post',
                    ],
                ]) . '</div>' . 
            '</div>',
        ];
    endforeach;
    echo SortableInput::widget([
        'name' => 'sorting',
        'value' => $weights,
        'items' => $images,
        'hideInput' => true,
        'options' => [
            'class' => 'form-control',
            'readonly' => true,
        ],
        'sortableOptions' => [
            'showHandle' => true,
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Назад'), ['view', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

    <?= Html::endForm() ?>

</div>
