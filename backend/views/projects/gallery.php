<?php
use yii\helpers\Html;
?>

<div id="gallery">
    <?php foreach ($model->images as $image): ?>
        <div class="gallery-image">
            <?= Html::img($image->path, [
                'title' => $image->title,
                'alt' => $image->title,
                'height' => 150,
                'class' => 'gallery-image',
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>