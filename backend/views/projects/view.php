<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Collapse;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Проекты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-pencil"></i> ' . Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-th"></i> ' . Yii::t('app', 'Галерея'), ['gallery', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить данный объект?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'statusLabel',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>
    
    <?= $model->body ?>
    
    <?= Collapse::widget([
        'items' => [
            [
                'label' => Yii::t('app', 'Изображения'),
                'content' => $this->render('gallery', [
                    'model' => $model,
                ]),
                'contentOptions' => [],
            ],
        ],
    ]) ?>

</div>
