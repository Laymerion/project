$(function() {
    if ($('.rich-editor').length > 0) {
        tinymceInit();
    }
});

function tinymceInit() {
    if (typeof tinymce != "undefined") {
        tinymce.init({
            mode: "specific_textareas",
            theme : "advanced",
            editor_selector : "rich-editor",
            width: '100%',
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,fontsizeselect,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,cleanup,code,|,forecolor,backcolor",
            theme_advanced_buttons2 : "tablecontrols,|,hr,charmap,image,media,pasteword"
        });
    }
}