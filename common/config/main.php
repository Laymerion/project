<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'formatter' => [
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'dateFormat' => 'php:d.m.Y',
        ],
    ],
];
