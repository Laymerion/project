<?php
return [
    'siteName' => 'Проект',
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'yiiRoot' => realpath(dirname(__FILE__).'/../../'),
    'host' => 'http://192.168.33.5',
];
