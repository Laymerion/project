<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $path
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ImagePreview[] $imagePreviews
 * @property Preview[] $previews
 * @property ProjectImage[] $projectImages
 * @property Project[] $projects
 */
class Image extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_BLOCKED = 'blocked';

    public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'path', 'status'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['path'], 'string', 'max' => 512],
            ['status', 'in', 'range' => [
                self::STATUS_BLOCKED,
                self::STATUS_PUBLISHED,
            ]],
            [
                ['file'],
                'file',
                'extensions' => 'jpg, jpeg, gif, png, tiff, bmp, cdr, cmx, psd, ai',
                'maxSize' => 1024 * 1024 * 30,
                'minSize' => 1024
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'description' => Yii::t('app', 'Описание'),
            'path' => Yii::t('app', 'Путь'),
            'status' => Yii::t('app', 'Статус'),
            'statusLabel' => Yii::t('app', 'Статус'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Обновлено'),
            'file' => Yii::t('app', 'Файл'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagePreviews()
    {
        return $this->hasMany(ImagePreview::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreviews()
    {
        return $this->hasMany(Preview::className(), ['id' => 'preview_id'])->viaTable(ImagePreview::tableName(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectImages()
    {
        return $this->hasMany(ProjectImage::className(), ['image_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id' => 'project_id'])->viaTable(ProjectImage::tableName(), ['image_id' => 'id']);
    }

    public function getFile()
    {
        return !empty($this->path) ? $this->path : '';
    }

    public static function getStatusLables()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('app', 'Опубликовано'),
            self::STATUS_BLOCKED => Yii::t('app', 'Не опубликовано'),
        ];
    }

    public function getStatusLabel()
    {
        $statuses = self::getStatusLables();
        return $statuses[$this->status];
    }

    public static function statusesList()
    {
        $statuses = self::getStatusLables();
        return array_merge(['' => '- ' . Yii::t('app', 'Выберите статус') . ' -'], $statuses);
    }

    /**
     * Get path to uploaded file.
     * @param \yii\web\UploadedFile $image
     * @return string
     */
    public function getNewPath(UploadedFile $image)
    {
        return '/content/images/' . $image->name;
    }

    /**
     * Create uploadedfile instance for uploaded file.
     * @return bool|\yii\web\UploadedFile
     */
    public function uploadImage() {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $image = UploadedFile::getInstance($this, 'file');

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }

        $image = self::renameFile($image, '/content/images/');

        return $image;
    }

    /**
     * Save image file and create thumbnails.
     * @param \yii\web\UploadedFile $image
     * @param string $additionalPath
     * 
     * @return array
     */
    public function upload(UploadedFile $image, $additionalPath = '')
    {
        $directory = '/content/images/' . $additionalPath;
        $image = self::renameFile($image, $directory);
        $yiiRoot = Yii::$app->params['yiiRoot'];
        \yii\helpers\FileHelper::createDirectory($yiiRoot . $directory);
        $filePath = $directory . $image->name;
        $fileAbsPath = $yiiRoot . $filePath;
        $image->saveAs($fileAbsPath);

        $thumbnails = $this->createThumbnails($image->name, $directory, $fileAbsPath);

        return [
            'path' => $filePath,
            'name' => $image->name,
            'thumbnails' => $thumbnails,
        ];
    }

    public static function renameFile(UploadedFile $image, $path)
    {
        $yiiRoot = Yii::$app->params['yiiRoot'];
        $directory = $yiiRoot . $path;
        $fileName = preg_replace("/[^\w\d\-\_\. ]/isu", '', $image->name);
        $fileName = preg_replace('/[\s]{2,}/', ' ', $fileName);
        $fileName = trim(strtolower(str_replace(' ', '_', $fileName)));
        $fileAbsPath = $directory . '/' . $fileName;
        $fileData = pathinfo($fileAbsPath);
        while (file_exists($fileAbsPath)) {
            $fileName = $fileData['filename'] . '_' . rand(0, 999) . '.' . $fileData['extension'];
            $fileAbsPath = $fileData['dirname'] . '/' . $fileName;
        }
        $image->name = $fileName;

        return $image;
    }

    /**
     * Thumbnails creation.
     * @param string $imageName
     * @param string $directory
     * @param string $filePath
     * 
     * @return array
     */
    public function createThumbnails($imageName, $directory, $filePath)
    {
        $yiiRoot = Yii::$app->params['yiiRoot'];
        $thumbnails = Preview::findAll(['status' => Preview::STATUS_PUBLISHED]);
        $img = \yii\imagine\Image::getImagine()->open($filePath);
        $size = $img->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        $thumbsDirectory = $directory . 'thumbs/';
        $thumbsAbsDirectory = $yiiRoot . $thumbsDirectory;

        \yii\helpers\FileHelper::createDirectory($thumbsAbsDirectory);

        $result = [];
        foreach ($thumbnails as $thumbnail) {
            if (!empty($thumbnail->width) && !empty($thumbnail->height)) {
                $thumbName = $thumbnail->width . '_' . $thumbnail->height . '_' . $imageName;
                $thumbWidth = ($width >= $thumbnail->width) ? $thumbnail->width : $width;
                $thumbHeight = ($height >= $thumbnail->height) ? $thumbnail->height : $height;
                \yii\imagine\Image::thumbnail($filePath, $thumbWidth, $thumbHeight)
                    ->save($thumbsAbsDirectory . $thumbName);
            }
            elseif (!empty($thumbnail->width) && empty($thumbnail->height)) {
                $thumbName = $thumbnail->width . '_0_' . $imageName;
                $thumbWidth = ($width >= $thumbnail->width) ? $thumbnail->width : $width;
                $thumbHeight = floor($height * ($thumbWidth / $width));
                \yii\imagine\Image::thumbnail($filePath, $thumbWidth, $thumbHeight)
                    ->save($thumbsAbsDirectory . $thumbName);
            }
            elseif (empty($thumbnail->width) && !empty($thumbnail->height)) {
                $thumbName = '0_' . $thumbnail->height . '_' . $imageName;
                $thumbHeight = ($height >= $thumbnail->height) ? $thumbnail->height : $height;
                $thumbWidth = floor($width * ($thumbHeight / $height));
                \yii\imagine\Image::thumbnail($filePath, $thumbWidth, $thumbHeight)
                    ->save($thumbsAbsDirectory . $thumbName);
            }
            $model = new ImagePreview;
            $model->setAttributes([
                'preview_id' => $thumbnail->id,
                'path' => $thumbsDirectory . $thumbName,
            ]);
            $result[] = $model;
        }

        return $result;
    }

    /**
     * Image file removing.
     * @param string $path
     */
    public static function removeFile($path)
    {
        if (!empty($path)) {
            $yiiRoot = Yii::$app->params['yiiRoot'];
            if (file_exists($yiiRoot . $path)) {
                unlink($yiiRoot . $path);
            }
        }
    }
}
