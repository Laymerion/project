<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "images_previews".
 *
 * @property integer $image_id
 * @property integer $preview_id
 * @property string $path
 *
 * @property Preview $preview
 * @property Image $image
 */
class ImagePreview extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images_previews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id', 'preview_id', 'path'], 'required'],
            [['image_id', 'preview_id'], 'integer'],
            [['path'], 'string', 'max' => 512],
            [['preview_id'], 'exist', 'skipOnError' => true, 'targetClass' => Preview::className(), 'targetAttribute' => ['preview_id' => 'id']],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => Image::className(), 'targetAttribute' => ['image_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image_id' => Yii::t('app', 'Изображение'),
            'preview_id' => Yii::t('app', 'Превью'),
            'path' => Yii::t('app', 'Путь'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreview()
    {
        return $this->hasOne(Preview::className(), ['id' => 'preview_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_id']);
    }
}
