<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "previews".
 *
 * @property integer $id
 * @property string $name
 * @property integer $width
 * @property integer $height
 * @property string $status
 *
 * @property ImagePreview[] $imagePreviews
 * @property Image[] $images
 */
class Preview extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_BLOCKED = 'blocked';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'previews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['width', 'height'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['status', 'in', 'range' => [
                self::STATUS_BLOCKED,
                self::STATUS_PUBLISHED,
            ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'width' => Yii::t('app', 'Ширина'),
            'height' => Yii::t('app', 'Высота'),
            'status' => Yii::t('app', 'Статус'),
            'statusLabel' => Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImagesPreviews()
    {
        return $this->hasMany(ImagePreview::className(), ['preview_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['id' => 'image_id'])->viaTable(ImagePreview::tableName(), ['preview_id' => 'id']);
    }

    public static function getStatusLables()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('app', 'Опубликовано'),
            self::STATUS_BLOCKED => Yii::t('app', 'Не опубликовано'),
        ];
    }

    public function getStatusLabel()
    {
        $statuses = self::getStatusLables();
        return $statuses[$this->status];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!isset($this->width) || empty($this->width)) {
            $this->width = 0;
        }
        if (!isset($this->height) || empty($this->height)) {
            $this->height = 0;
        }
        return parent::beforeSave($insert);
    }

    public function rebuildImages()
    {
        $yiiRoot = Yii::$app->params['yiiRoot'];
        Yii::$app->db->createCommand()
            ->delete(ImagePreview::tableName(), ['preview_id' => $this->id])
            ->execute();
        $images = Image::find()->all();
        $counter = 0;
        foreach ($images as $image) {
            /* @var $image Image */
            $pathinfo = pathinfo($image->path);
            $directory = $pathinfo['dirname'] . '/';
            \yii\helpers\FileHelper::createDirectory($yiiRoot . $directory);
            $thumbs = $image->createThumbnails($pathinfo['basename'], $directory, $yiiRoot . $image->path);
            foreach ($thumbs as $thumb) {
                $thumb->setAttribute('image_id', $image->id);
                $thumb->save();
            }
            $counter++;
        }
        Yii::$app->session->setFlash('success', Yii::t('app', 'Изображений обновлено: {count}', ['count' => $counter]));
    }
}
