<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;
use yii\db\Query;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $name
 * @property string $body
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $date
 *
 * @property ProjectImage[] $projectImages
 * @property Image[] $images
 */
class Project extends \yii\db\ActiveRecord
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_BLOCKED = 'blocked';
    const FILE_PATH = '';

    public $imagesList;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'date'], 'required'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'in', 'range' => [
                self::STATUS_PUBLISHED,
                self::STATUS_BLOCKED,
            ]],
            [['imagesList', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'date' => Yii::t('app', 'Дата'),
            'body' => Yii::t('app', 'Описание'),
            'status' => Yii::t('app', 'Статус'),
            'statusLabel' => Yii::t('app', 'Статус'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Обновлено'),
            'imagesList' => Yii::t('app', 'Изображения'),
            'imagesCount' => Yii::t('app', 'Кол-во изображений'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        foreach ($this->images as $image) {
            $this->imagesList[] = $image;
        }
        $this->date = Yii::$app->formatter->asDate($this->date);
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectImages()
    {
        return $this->hasMany(ProjectImage::className(), ['project_id' => 'id'])->orderBy(['weight' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['id' => 'image_id'])->viaTable(ProjectImage::tableName(), ['project_id' => 'id']);
    }

    public static function getStatusLables()
    {
        return [
            self::STATUS_PUBLISHED => Yii::t('app', 'Опубликовано'),
            self::STATUS_BLOCKED => Yii::t('app', 'Не опубликовано'),
        ];
    }

    public static function statusesList()
    {
        $statuses = self::getStatusLables();
        return array_merge(['' => '- ' . Yii::t('app', 'Выберите статус') . ' -'], $statuses);
    }

    public function getStatusLabel()
    {
        $statuses = self::getStatusLables();
        return $statuses[$this->status];
    }

    public function getImagesCount()
    {
        return sizeof($this->images);
    }

    /**
     * @param \yii\web\UploadedFile $image
     */
    public function uploadImage(UploadedFile $image)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $imageModel = new Image;
            $imageData = $imageModel->upload($image, 'projects/' . $this->id . '/');
            $imageModel->setAttributes([
                'title' => $imageData['name'],
                'description' => '',
                'path' => $imageData['path'],
                'status' => Image::STATUS_PUBLISHED,
            ]);
            $imageModel->save();

            foreach ($imageData['thumbnails'] as $imagePreviewModel) {
                $imagePreviewModel->image_id = $imageModel->id;
                $imagePreviewModel->save();
            }

            $relationModel = new ProjectImage;
            $relationModel->setAttributes([
                'project_id' => $this->id,
                'image_id' => $imageModel->id,
                'weight' => 0,
                'title' => $imageData['name'],
                'description' => '',
            ]);
            $relationModel->save();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!is_numeric($this->date)) {
            $this->date = strtotime($this->date);
        }
        return parent::beforeSave($insert);
    }

    /**
     * Get list of projects.
     * @param boolean $publicOnly
     * @return array
     */
    public static function getAllProjects($publicOnly = true)
    {
        $query = new Query;
        $fields = [
            'id',
            'name',
            'date',
            'created_at',
            'status',
        ];
        $query->select($fields)->from(self::tableName());
        if ($publicOnly) {
            $query->where(['status' => self::STATUS_PUBLISHED]);
        }
        $query->orderBy(['date' => SORT_DESC]);
        return $query->all();
    }

    /**
     * Get all data about projects by ID
     * @param integer $projectId
     * @return array
     */
    public static function getProjectData($projectId)
    {
        $query = new Query;
        $fields = [
            'id',
            'name',
            'body',
            'date',
            'created_at',
            'updated_at',
        ];
        $query->select($fields)
            ->from(self::tableName())
            ->where(['id' => $projectId]);
        return $query->one();
    }

    /**
     * Get proect images count by id.
     * @param integer $projectId
     * @return integer
     */
    public static function getProjectImagesCount($projectId)
    {
        $query = new Query;
        $query->select('COUNT(*)')
            ->from(ProjectImage::tableName())
            ->where(['project_id' => $projectId]);
        return $query->scalar();
    }

    /**
     * Get info about all project images.
     * @param integer $projectId
     * @param string $preview
     * @return array
     */
    public static function getAllProjectImages($projectId, $preview = 'original')
    {
        $previewName = (!empty($preview) && ($preview != 'original')) ? $preview : '';

        $query = new Query;
        $fields = [
            'i.id',
            'i.title',
            'i.description',
            'original_image' => 'i.path',
            'i.status',
        ];
        if (!empty($previewName)) {
            $fields['preview_image'] = 'ip.path';
        }
        $query->select($fields)
            ->from(ProjectImage::tableName() . ' pi')
            ->leftJoin(Image::tableName() . ' i', 'i.id = pi.image_id');
        $query->where([
                'pi.project_id' => $projectId,
                'i.status' => Image::STATUS_PUBLISHED,
            ])
            ->orderBy(['pi.weight' => SORT_ASC]);
        if (!empty($previewName)) {
            $query->leftJoin(ImagePreview::tableName() . ' ip', 'ip.image_id = i.id')
                ->leftJoin(Preview::tableName() . ' p', 'p.id = ip.preview_id')
                ->andWhere(['p.name' => $previewName]);
        }
        return $query->all();
    }
}
