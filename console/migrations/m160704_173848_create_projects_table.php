<?php

use yii\db\Migration;

/**
 * Handles the creation for table `projects_table`.
 */
class m160704_173848_create_projects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'body' => $this->text(),
            'status' => $this->string(10)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('projects_name_index', 'projects', 'name');
        $this->createIndex('projects_status_index', 'projects', 'status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('projects_table');
    }
}
