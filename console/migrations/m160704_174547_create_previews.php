<?php

use yii\db\Migration;

/**
 * Handles the creation for table `previews`.
 */
class m160704_174547_create_previews extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('previews', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'width' => $this->integer()->notNull()->defaultValue(0),
            'height' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->string(10),
        ], $tableOptions);

        $this->createIndex('previews_name_index', 'previews', 'name');
        $this->createIndex('previews_status_index', 'previews', 'status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('previews');
    }
}
