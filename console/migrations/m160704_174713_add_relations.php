<?php

use yii\db\Migration;

class m160704_174713_add_relations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('projects_images', [
            'project_id' => $this->integer()->notNull(),
            'image_id' => $this->integer()->notNull(),
            'weight' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);
        $this->addPrimaryKey('PRIMARY KEY', 'projects_images', ['project_id', 'image_id']);
        $this->createIndex('projects_images_weight_index', 'projects_images', 'weight');
        $this->createIndex('projects_images_image_id_index', 'projects_images', 'image_id');
        $this->addForeignKey('fk_projects_images_project_id', 'projects_images', 'project_id', 'projects', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_projects_images_image_id', 'projects_images', 'image_id', 'images', 'id', 'cascade', 'cascade');

        $this->createTable('images_previews', [
            'image_id' => $this->integer()->notNull(),
            'preview_id' => $this->integer()->notNull(),
            'path' => $this->string(512)->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('PRIMARY KEY', 'images_previews', ['image_id', 'preview_id']);
        $this->createIndex('images_previews_preview_id_index', 'images_previews', 'preview_id');
        $this->addForeignKey('fk_images_previews_image_id', 'images_previews', 'image_id', 'images', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_images_previews_preview_id', 'images_previews', 'preview_id', 'previews', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fk_projects_images_image_id', 'projects_images');
        $this->dropForeignKey('fk_projects_images_project_id', 'projects_images');
        $this->dropTable('projects_images');

        $this->dropForeignKey('fk_images_previews_preview_id', 'images_previews');
        $this->dropForeignKey('fk_images_previews_image_id', 'images_previews');
        $this->dropTable('images_previews');
    }
}
