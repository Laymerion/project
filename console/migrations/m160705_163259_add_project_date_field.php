<?php

use yii\db\Migration;

class m160705_163259_add_project_date_field extends Migration
{
    public function up()
    {
        $this->addColumn('projects', 'date', $this->integer()->notNull());
        $this->createIndex('projects_date_index', 'projects', 'date');
    }

    public function down()
    {
        $this->dropColumn('projects', 'date');
    }
}
