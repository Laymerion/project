<?php

use yii\db\Migration;

class m160913_154428_add_project_image_data extends Migration
{
    public function up()
    {
        $this->addColumn('projects_images', 'title', $this->string(255)->notNull());
        $this->addColumn('projects_images', 'description', $this->text());
    }

    public function down()
    {
        $this->dropColumn('projects_images', 'title');
        $this->dropColumn('projects_images', 'description');
    }
}
